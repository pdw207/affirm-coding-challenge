import 'babel-polyfill';
import CSVDataSource from '../../src/database/CSVDataSource'
import path from 'path';
let database;

describe('CSVDataSource', () => {
  describe('#seed', () => {
    beforeAll(async ()=>{
      const inputPath = path.join(__dirname, '../fixtures/input');
      database = new CSVDataSource(inputPath);
      await database.load()
    })

    it('load csv data to create in-memory database', ()=> {
      expect(database.data.banks).toEqual([
        {id: "1", name: "Chase"},
        {id: "2", name: "Bank of America"}
      ])
    });

    it('transforms by type defined in schema', async ()=> {
      const sampleFixture = database.data.facilities[0]

      expect(typeof sampleFixture.bankId).toEqual('string')
      expect(typeof sampleFixture.id).toEqual('string')
      expect(typeof sampleFixture.amount).toEqual('number')
      expect(typeof sampleFixture.interestRate).toEqual('number')
    })
  })
});