import "babel-polyfill";
import LoanApplicationReview from "../src/LoanApplicationReview";
import _ from "lodash";

describe("LoanApplicationReview", () => {
  describe("#findDebtFacility", () => {
    it("returns facility if no applicable covenants ", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [ { "id": "1", "amount": 1779071, "interestRate": 0.04, "bankId": "1" }]

      const application = new LoanApplicationReview(request, facilities, []);
      expect(application.findDebtFacility()).toEqual(facilities[0]);
    });

    it("returns null if facility value is insufficient ", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [ { "id": "1", "amount": 1000, "interestRate": 0.04, "bankId": "1" }]

      const application = new LoanApplicationReview(request, facilities, []);
      expect(application.findDebtFacility()).toEqual(null);

    });
    it("returns null if restricted by region", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [
        { "id": "1", "amount": 1779071, "interestRate": 0.04},
      ]
      const covenants = [{
        "facilityId": facilities[0].id,
        "bannedState": request.state
      }]

      const regionalBannedApplication = new LoanApplicationReview(request, facilities, covenants);
      expect(regionalBannedApplication.findDebtFacility()).toEqual(null);

      covenants[0].bannedState = 'OTHER'

      const applicationWithUpdatedCovenants = new LoanApplicationReview(request, facilities, covenants);
      expect(applicationWithUpdatedCovenants.findDebtFacility()).toEqual(facilities[0]);


    });
    it("returns null if maximum covenant risk exceeds default likelihood", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [
        { "id": "1", "amount": 1779071, "interestRate": 0.04},
      ]
      const covenants = [{
        "facilityId": facilities[0].id,
        "maxDefaultLikelihood": 0.01
      }]

      const riskyApplication = new LoanApplicationReview(request, facilities, covenants);
      expect(riskyApplication.findDebtFacility()).toEqual(null);

      covenants[0].maxDefaultLikelihood = 0.5

      const applicationWithUpdatedCovenants = new LoanApplicationReview(request, facilities, covenants);
      expect(applicationWithUpdatedCovenants.findDebtFacility()).toEqual(facilities[0]);
    });

    it("returns null where facility_id is null and covenants applies to bank", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [ { id: "1", amount: 1779071, interestRate: 0.04, bankId: "1" }]
      const covenants = [{
        facilityId: null,
        bankId: facilities[0].bankId,
        maxDefaultLikelihood: 0.01
      }]

      const application = new LoanApplicationReview(request, facilities, covenants);
      expect(application.findDebtFacility()).toEqual(null);
    });

    it("returns null if unprofitable", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 23549,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [ { id: "1", amount: 1779071, interestRate: 0.3, bankId: "1" }]
      const covenants = [{
        facilityId: facilities[0].id,
      }]

      const application = new LoanApplicationReview(request, facilities, covenants);

      expect(application.findDebtFacility()).toEqual(null);
      expect(application._calculateExpectedYield(facilities[0])).toBeLessThan(0)

      facilities[0].interestRate = 0.01

      expect(application._calculateExpectedYield(facilities[0])).toBeGreaterThan(0)
    });

    it("returns lowest cost facility if more than one is available", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 100,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [
        { id: "1", amount: 200, interestRate: 0.10 },
        { id: "2", amount: 200, interestRate: 0.04 },
        { id: "3", amount: 200, interestRate: 0.09 },
        { id: "4", amount: 200, interestRate: 0.07 }
      ]

      const application = new LoanApplicationReview(request, facilities, []);

      expect(application.findDebtFacility().interestRate).toEqual(0.04);
    });
    it("returns lowest value debt facility if costs are equal", () => {
      const request = {
        id: "1",
        interestRate: 0.25,
        amount: 100,
        defaultLikelihood: 0.04,
        state: "LA"
      };

      const facilities = [
        { id: "1", amount: 200, interestRate: 0.05 },
        { id: "2", amount: 150, interestRate: 0.05 },
        { id: "3", amount: 100, interestRate: 0.05 },
        { id: "4", amount: 400, interestRate: 0.05 }
      ]

      const application = new LoanApplicationReview(request, facilities, []);

      expect(application.findDebtFacility().amount).toEqual(100);
    });
  });
});
