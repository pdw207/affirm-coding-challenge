# Coding Challenge
The coding challenge was completed using nodejs with babel which supports ES6 syntax,
tests use jest, and fixtures are hardcoded. The output is saved in the data folder.

## Getting Started
Assuming you are using [`yarn`](https://yarnpkg.com/lang/en/) as your package
manager, run `yarn` to install necessary dependencies and run with:

```
$ yarn start
```

To run the test suite:

```
$ yarn test
```
# Questions

## How long did you spend working on the problem? What did you find to be the most difficult part?

  I spent at least 5 hours working on the exercise. The most difficult part for me was capturing the structure and behavior of the system in a logical way.

## How would you modify your data model or code to account for an eventual introduction of new, as-of-yet unknown types of covenants, beyond just maximum default likelihood and state restrictions?

  I think the LoanApplicationReview Service should have a limited understanding of the exact requirements of the underlying covenants and it should be handled outside of the class. Further the implicit association of a covenants to a bank, and lack of effective dates for these associations highlight an area of possible risk.

## How would you architect your solution as a production service wherein new facilities can be introduced at arbitrary points in time. Assume these facilities become available by the finance team emailing your team and describing the addition with a new set of CSVs.

  If we found it prohibitive to move to a relational database, we would want to implement several necessary features. These would include persisting data to disk after each transaction, row (and possibly table) locking, data validation (missing or timing of persisting covenant), and some type of ingest workflow. So in this case if we automate the creation of records from an email we will could save to a dedicated temp folder on the file system, and remove rows that have been validated and persisted to the input folder, those entries that are not valid could remain in that folder. It may make sense to introduce a pending/active state for facilities, locks and a review process.

## Your solution most likely simulates the streaming process by directly calling a method in your code to process the loans inside of a for loop. What would a REST API look like for this same service? Stakeholders using the API will need, at a minimum, to be able to request a loan be assigned to a facility, and read the funding status of a loan, as well as query the capacities remaining in facilities.

  Each resource would have a CRUD action exposed publicly with an authenticated endpoint, with nesting no longer than two resources. To flesh out the endpoints you referred to we can initiate a `POST to /facilities/:facilityId/loans/:loanId` to request a loan be assigned to a facility, `GET to /loans/:loanId` to read the funding status of a loan and  `GET to /facilities` to query the capacities remaining in the facilities (if I understand that use case correctly). Each endpoint could support query string options for resource expansion, pagination and filtering and [web linking](https://tools.ietf.org/html/rfc5988) could be leveraged to expose the location of associations through a `href` attribute.
  ```

## How might you improve your assignment algorithm if you were permitted to assign loans in batch rather than streaming? We are not looking for code here, but pseudo code or description of a revised algorithm appreciated.

  It feels like it might be similar to the Knapsack Problem but may need to consider the problem a little further. A possible solution is to consider all the possible subsets of combinations that fulfill the requirements of the covenants and take the optimal solution. In each step we would be looking at the most profitable available loan.

## Discuss your solution’s runtime complexity.

  I did not implement caching, memoization or introduce specific data structures to improve runtime performance for large datasets.  Worst case its cubic complex with three nested loops (looping through each loan, looping all available facilities and then covenants with the `includes`). On average the runtime complexity is probably closer to quadratic as covenants applicable to a given loan request are probably small.