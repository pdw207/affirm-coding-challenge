import LoanApplicationReview from './LoanApplicationReview';

export default class LoanApplicationProcessor {
  constructor(database) {
    this.database = database;
  }
  start(){
    const { loans, facilities, covenants } = this.database.data;
    console.log('starting loan processing...');

    loans.forEach((loan)=> {
      const application = new LoanApplicationReview(loan, facilities, covenants);
      const facility = application.findDebtFacility();

      if(facility){
        // FIXME Move to database as #save or #update
        const idx = facilities.findIndex(f => f.id === facility.id);
        facilities[idx].amount -= loan.amount;
        facilities[idx].expectedYield +=application.expectedYield();
        loan.facilityId = facility.id;
      } else {
        console.log('loan unfunded');
      }
    })

  }
}