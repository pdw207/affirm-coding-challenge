// Schema set types for Schema functions tell how to format the csv values.
const uuid = require('uuid');

const bankSchema = el => ({
  id: el.id,
  name: el.name
});

const covenantSchema = el => ({
  id: uuid.v4(), // FIXME: There is no id for covenant so this is a placeholder
  facilityId: el.facility_id,
  maxDefaultLikelihood: parseFloat(el.max_default_likelihood),
  bankId: el.bank_id,
  bannedState: el.banned_state
});

const facilitySchema = el => ({
  id: el.id,
  amount: parseInt(el.amount),
  interestRate: parseFloat(el.interest_rate),
  bankId: el.bank_id,
  expectedYield: 0
});

const loanSchema = el => ({
    id: el.id,
    interestRate: parseFloat(el.interest_rate),
    amount: parseInt(el.amount),
    defaultLikelihood: parseFloat(el.default_likelihood),
    state: el.state,
    facilityId: null
});

const entityMap = entity => {
  return {
    banks: bankSchema,
    covenants: covenantSchema,
    facilities: facilitySchema,
    loans: loanSchema,
  }[entity]
}
export default entityMap