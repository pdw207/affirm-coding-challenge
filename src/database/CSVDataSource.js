import fs from 'fs';
import path from 'path';
import csv from 'csvtojson';
import entityMap from './Schema';
const writeCSV = require('csv-writer').createObjectCsvWriter;

/** Database seeded from the local file system
* NOTE: move to RDBMS as database grows
*/
export default class CSVDataSource {
  /**
  * @param {string} directory - path to collection of csv files
  */
  constructor(inputPath, outputPath) {
    this.inputPath = inputPath;
    this.outputPath = outputPath;
    this.data = {};
  }

  /**
   * Write csv to output directory
   */
  write(fileName, table, columns) {
    const writer = writeCSV({
      path: `${this.outputPath}/${fileName}.csv`,
      header: columns.map(field => ({id: field, title: field}))
    });

    writer
      .writeRecords(this.data[table])
      .then(()=> console.log(`saved ${fileName} to the output directory`));

  }

  /**
   * Load data in-memory from csv files
   */
  load() {
    return Promise.all(
      fs
        .readdirSync(this.inputPath)
        .filter( f => path.extname(f) === '.csv')
        .map(file => this._loadTransformedDataFromCSV(file))
    )
  }
  /**
  * loads csv and applies types defined in schema
  * @param {string} fileName - name including extension
  */
  async _loadTransformedDataFromCSV(fileName){
    const entity = path.basename(fileName, '.csv');
    const absolutePath = path.join(this.inputPath,fileName);

    let data = await csv().fromFile(absolutePath);

    this.data[entity] = data.map(
      this._entitySchema(entity)
    );
  }
  /**
  * Validate and apply schema
  * @param {string} entity - name of entity
  */
  _entitySchema(entity) {
    let schema = entityMap(entity);
      if(!schema) {
        throw new Error(`Schema not found for ${entity}`);
      }
      return schema
  }
}
