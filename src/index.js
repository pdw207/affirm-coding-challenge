import 'babel-polyfill';
import CSVDataSource from './database/CSVDataSource';
import LoanApplicationProcessor from './loanApplicationProcessor'
import path from 'path';

const inputDirectory = path.join(__dirname, '../data/input');
const outputDirectory = path.join(__dirname, '../data/output');
let database = new CSVDataSource(inputDirectory, outputDirectory);

database.load().then(() => {
  const process = new LoanApplicationProcessor(database);

  process.start();

  console.log('processing completed');

  database.write('yield', 'facilities', ['id', 'expectedYield']);
  database.write('assignments', 'loans', ['id', 'facilityId']);
})


