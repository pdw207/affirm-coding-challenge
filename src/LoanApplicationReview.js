import { loadOptions } from "@babel/core";
import _ from 'lodash';

export default class LoanApplicationReview {
  /**
  * @param {object} data - loan request information
  * @param {array<object>} facilities - debt facilities
  * @param {array<object>} covenants - available covenants that relate to facility and bank
  */
  constructor(data, facilities, covenants) {
    this._data = data;

    // Greedy approach: facilities are sorted by desc interestRate asc amount
    this._sortedDebtFacilities = _.sortBy(facilities,[
      f => f.interestRate,
      f => f.amount
    ]);
    this._covenants = covenants;
    this.debtFacility = null;
  }
  /**
  *  Identifies available debt facility for loan request
  */
  findDebtFacility() {
    const applicableCovenantRestrictions = this._covenants
      .filter(({maxDefaultLikelihood, bannedState }) => {
        const { state, defaultLikelihood } = this._data;
        const regionalRestriction = bannedState === state;
        const highRisk = maxDefaultLikelihood ? defaultLikelihood > maxDefaultLikelihood : false

        return regionalRestriction || highRisk
      })

    // Collect references to facility covenant restrictions
    const facilityCovenantRestrictionIds = applicableCovenantRestrictions
      .filter(c => c.facilityId !== null)
      .map(c => c.facilityId);

    // those covenants with missing facility ids apply to the bank as a whole
    const bankCovenantRestrictionIds = applicableCovenantRestrictions
      .filter(covenant => covenant.facilityId === null)
      .map(covenant => covenant.bankId);

    this.assignedFacility = _.find(this._sortedDebtFacilities, facility => {
      return facility.amount >= this._data.amount &&
       this._isProfitable(facility) &&
        !bankCovenantRestrictionIds.includes(facility.bankId) &&
        !facilityCovenantRestrictionIds.includes(facility.id)
    }) || null; // explicitly return null instead of undefined

    return this.assignedFacility;
  }


  _isProfitable(facility) {
    return this._calculateExpectedYield(facility) >= 0;
  }

  expectedYield() {
    return this._calculateExpectedYield(this.assignedFacility)
  }

  _calculateExpectedYield(facility) {
    const { defaultLikelihood, amount, interestRate } = this._data;
    const expectedInterest = (1 - defaultLikelihood) * interestRate * amount;
    const expectedLossFromDefault = defaultLikelihood * amount;
    const facilityCost = facility.interestRate * amount;

    return Math.round(expectedInterest - expectedLossFromDefault - facilityCost);
  }
}